# Editor Icons for Mapbase

To install, you will have to convert everything to VTF/VMT, put them in `materials\editor`, and edit the FGD to reference them.

This is a project in association with [Blixibon](https://github.com/Blixibon), and hopefully some of these will be incorporated into Mapbase :)

Also of relevance is a Paint.NET plugin I made with CodeLab, [Additive Texture to Transparent](https://gitlab.com/moofemp-pdn-plugins/additive-texture-to-transparent). This is helpful for when you need to use an additive texture on top of a transparent background, since PDN's Additive blend mode doesn't normally do this.

\- Holly
